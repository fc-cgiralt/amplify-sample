import { Component } from '@angular/core';
import { AmplifyService } from 'aws-amplify-angular';
import Analytics from '@aws-amplify/analytics';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'amplify-sample';

  signedIn: boolean;
  user: any;
  greeting: string;

  usernameAttributes = "email";

  signUpConfig = {
    header: 'My Customized Sign Up',
    hideAllDefaults: true,
    defaultCountryCode: '1',
    signUpFields: [
      {
        label: 'Username',
        key: 'username',
        required: true,
        displayOrder: 1,
        type: 'string',
      },
      {
        label: 'Email',
        key: 'email',
        required: true,
        displayOrder: 2,
        type: 'string',
      },
      {
        label: 'Password',
        key: 'password',
        required: true,
        displayOrder: 3,
        type: 'password'
      },
      {
        label: 'Phone Number',
        key: 'phone_number',
        required: true,
        displayOrder: 4,
        type: 'string'
      }
    ]
  }

  constructor(private amplifyService: AmplifyService) {
    this.amplifyService.authStateChange$.subscribe(authState => {
      this.signedIn = authState.state === 'signedIn';
      if (!authState.user) {
        this.user = null;
      } else {
        this.user = authState.user;
        this.greeting = 'Hello ' + this.user.username;
        if (this.signedIn) {

          const analyticsConfig = {
            AWSPinpoint: {
              // Amazon Pinpoint App Client ID
              appId: 'fad75e17a7c14b928eb0e4c9709bdf8b',
              // Amazon service region
              region: 'us-west-2',
              mandatorySignIn: false,
            }
          }
          Analytics.configure(analyticsConfig);
          Analytics.record('login', this.user.username);
        }
      }
    });
  }

  sendEvent() {
    Analytics.record({
      name: 'button click',
      attributes: { username: this.user.username }
    }).then(
      () => { console.log('button clicked'); }
    );
  }
}
